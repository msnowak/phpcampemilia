<?php

require_once 'ConfigDB.php';

class Database {
    static private $host = DB_HOST;
    static private $user = DB_USER;
    static private $password = DB_PASSWORD;
    static private $name = DB_DATABASE;

    public static $pdo;

    public function __construct()
    {
        $this->getConnection();
    }

    public static function getConnection()
    {

        if (self::$pdo == NULL) {
            $dsn = 'mysql:host=' . self::$host . ';dbname=' . self::$name;

            $options = array(
                //trwałe połączenie
                PDO::ATTR_PERSISTENT => true,
                //obsługa błędów
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            );

            try {
                self::$pdo = new PDO($dsn, self::$user, self::$password, $options);
            } catch (PDOException $e) {
                $e->getMessage();
            }
        }
        return self::$pdo;
    }
}