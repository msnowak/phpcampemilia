<?php

require_once 'Database.php';

class ProductRepository {

    public function checkProduct($id) {
        $result = NULL;
        $pdo = Database::getConnection();
        $sql = "SELECT * FROM `products` WHERE `id`='$id';";
        try {
            $stmt = $pdo->prepare($sql);
            $params = array();
            $stmt->execute($params);
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "<br><br><b>PDOException: $e</b>";
        }
        return $result;
    }

    public function addProduct($name, $price) {
        $pdo = Database::getConnection();
        $sql = "INSERT INTO `products` (`id`, `nazwa`, `cena`) VALUES (NULL, '$name', '$price');";
        try {
            $stmt = $pdo->prepare($sql);
            $params = array();
            $stmt->execute($params);
            $result = "Poprawnie wykonano dodanie do bazy danych";
        } catch (PDOException $e) {
            echo "<br><br><b>PDOException: $e</b>";
            $result = "Nie udało się dodać elementu do bazy danych";
        }
        return $result;
    }

    public function removeProduct($id) {
//        $available = $this->checkProduct($id);
//        if ($available != NULL) {
            $pdo = Database::getConnection();
            $sql = "DELETE FROM `products` WHERE `id`='$id';";
            try {
                $stmt = $pdo->prepare($sql);
                $params = array();
                $stmt->execute($params);
                $result = "Poprawnie wykonano usunięcie z bazy danych";
            } catch (PDOException $e) {
                echo "<br><br><b>PDOException: $e</b>";
                $result = "Nie udało sie usunąć elementu z bazy danych";
            }
//        } else {
//            $result = "Brak danego elementu w bazie danych";
//        }
        return $result;
    }

    public function fetchAll() {
        $result = NULL;
        $pdo = Database::getConnection();
        $sql = "SELECT * FROM `products` ORDER BY `id` ASC;";
        try {
            $stmt = $pdo->prepare($sql);
            $params = array();
            $stmt->execute($params);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "<br><br><b>PDOException: $e</b>";
        }
        return $result;
    }
}