<?php
header('Content-type: text/xml');

require_once 'ProductRepository.php';

class ServiceFunctions {
    public function checkProduct($id) {
        $product = ProductRepository::checkProduct($id);
        //echo json_encode($product);
        try {
            echo '<?xml version="1.0" ?><product></product>';
            $simplexml = new SimpleXMLElement('<product></product>');
//            foreach ($products as $product) {
                $simplexml->addChild('product', $product['nazwa']);
//            }
            echo xml_get_error_code();
            return $simplexml->asXML();
        } catch (Exception $e) {
            echo "<br><br><b>XMLException: $e</b>";
        }
        exit();
    }

    public function addProduct($name, $price) {
        return ProductRepository::addProduct($name, $price);
    }

    public function removeProduct($id) {
        return ProductRepository::removeProduct($id);
    }
}

$options = array('uri' => 'http://localhost/');
$server = new SoapServer(NULL, $options);
$server->setClass('ServiceFunctions');
$server->handle();