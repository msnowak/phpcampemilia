<?php
header('Content-type: text/xml');

require_once 'ProductRepository.php';

$options = array (
    'uri' => 'http://localhost',
    'location' => 'http://localhost/phpcamp/SOA/soap_server.php',
    'trace' => 1
);

try {
    $client = new SoapClient(NULL, $options);
} catch (Exception $e) {
    var_dump($e->getMessage());
    var_dump($client->__getLastRequest());
    var_dump($client->__getLastResponse());
}

//$products = ProductRepository::fetchAll();
//foreach ($products as $product) {
//    echo "<br>";
//    print_r($product);
//}

echo "<br><br>Check product: " . simplexml_load_string($client->checkProduct(5));
echo "<br><br>Add product: " . $client->addProduct('czapka', '49.99');
echo "<br><br>Remove product: " . $client->removeProduct(6) . "<br>";

$products = ProductRepository::fetchAll();
foreach ($products as $product) {
    echo "<br>";
    print_r($product);
}