<?php

class ProductClass {
    protected $id;
    protected $name;
    protected $price;

    public function __construct() {
    }

    public function getProduct($product) {
        $this->id = $product['id'];
        $this->name = $product['nazwa'];
        $this->price = $product['cena'];
    }
}