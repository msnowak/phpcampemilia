<?php

require_once 'Product.php';

$timeStart = microtime(true);
echo 'Peak: ' . memory_get_peak_usage()/1000 . ' kB<br><br>';

$product = new Product(1, 'produkt' . $i, 8.99 + $i, 'PLN', 'jakaś rzecz ' . $i, 'produkt' . $i . '.jpg', 'producent' . $i, 'fajna rzecz' . $i);
$productVirtual = new Product(2, 'produkt' . $i, 8.99 + $i, 'PLN', 'jakaś rzecz ' . $i, 'produkt' . $i . '.jpg', 'producent' . $i, 'ebook' . $i);

$product->setQuantity(10);
$product->setWeight(5.5);
$productVirtual->setAttachment('zalacznik');
echo $product->getQuantity() . '<br>';
echo $product->getWeight() . '<br>';
echo $productVirtual->getAttachment() . '<br>';

echo '<br>Peak: ' . memory_get_peak_usage()/1000 . ' kB<br>';
$time = round((microtime(true) - $timeStart), 4);
echo 'Time: ' . $time;