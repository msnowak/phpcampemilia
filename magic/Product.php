<?php

class Product {

    public function __construct($_id, $_name, $_price, $_currency, $_description, $_pictures, $_manufacturers, $_category) {
        $this->setId($_id);
        $this->setName($_name);
        $this->setPrice($_price);
        $this->setCurrency($_currency);
        $this->setDescription($_description);
        $this->setPictures($_pictures);
        $this->setManufacturer($_manufacturers);
        $this->setCategory($_category);
    }

    //brak działania przy public i private
    public function __set($name, $value) {
        $this->$name = $value;
        return $this;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    //ostrzeżenie przed użyciem niedostępnych fukcji
    //argumenty przekazane jako tablica
    //próbuje ustawić sety
    public function __call($name, $arguments) {
        $matches = [];
        if (preg_match('/set([A-Za-z]{1,})/', $name, $matches)) {
            if (isset($matches[0])) {
                $argument = strtolower(substr($matches[0], 3));
                $this->$argument = $arguments[0];
            }
        } elseif (preg_match('/get([A-Za-z]{1,})/', $name, $matches)) {
            if (isset($matches[0])) {
                $argument = strtolower(substr($matches[0], 3));
                return $this->$argument;
            }
        } else {
            echo "Calling object method '$name' "
                . implode(', ', $arguments) . "\n";
        }
    }

    public static function __callStatic($name, $arguments)
    {
        echo "Calling static method '$name' "
            . implode(', ', $arguments). "\n";
    }
}