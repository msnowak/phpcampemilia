<?php


require_once 'DB.interface.php';

class DB implements iDB
{
    public $link;
    public $queryResult;

    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

    public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);

        return (bool)$this->queryResult;
    }

    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    public function getRow()
    {
        if (!$this->queryResult) {
            throw new Exception('Zapytanie nie zostalo wykonane');
        }

        return mysqli_fetch_assoc($this->queryResult);
    }

    public function getAllRows()
    {
        $results = array();

        while (($row = $this->getRow())) {
            $results[] = $row;
        }

        return $results;
    }
}