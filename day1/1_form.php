<?php

//sprawdzać typ i format danych
//htmlspecialchars($text)
//rzutowanie na odpowiedni typ danych (int) (string), itd. -> mysqli_real_escape_string

//sprawdzać zakres danych
//sprawdzać czy dane nie zawierają złośliwego kodu (SQL Injection, XSS)

//Tokeny:
//1 min
//nie może być oczywisty
//sprawdzać poprawność tokenu i jego ważności
//time()
//md5

?>

<form method="post" action="database.php">
    <label for="name">Imię</label>
    <input type="text" name="name" id="name"><br>
    <label for="surname">Nazwisko</label>
    <input type="text" name="surname" id="surname"><br>
    <label for="gender">Płeć</label>
    <select name="gender" id="gender">
        <option value="male">Mężczyzna</option>
        <option value="female">Kobieta</option>
        <option value="unknown">Nieznana</option>
    </select><br>
    <label for="date_of_birth">Data urodzenia</label>
    <input type="date" name="date_of_birth" id="date_of_birth"><br>
    <label for="orders_count">Ilość zamówień</label>
    <input type="number" name="orders_count" id="orders_count" value="1"><br>
    <label for="street">Ulica</label>
    <input type="text" name="street" id="street"><br>
    <label for="city">Miasto</label>
    <input type="text" name="city" id="city"><br>
    <label for="postcode">Kod pocztowy</label>
    <input type="text" name="postcode" id="postcode"><br>
    <label for="country">Państwo</label>
    <input type="number" name="country" id="country" value="1"><br>
    <label for="notes">Notatki</label>
    <textarea name="notes" id="notes"></textarea><br>
    <input type="submit" name="button" value="Wyślij!">
</form>
